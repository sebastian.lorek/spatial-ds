"""
Shiny app.
"""

# Dependencies
from shiny import ui, render, App, reactive
from rain import knn_smooth, plot_points, plot_interpolation
import os
import subprocess
import pickle
import pandas as pd
import pygadm
import ssl
import asyncio

# Disable SSL verification
ssl._create_default_https_context = ssl._create_unverified_context

print(os.getcwd())

# Call the data_pipe.py script 
command = ['python', 'data_pipe.py']
process = subprocess.Popen(command)

# Wait for the process to finish and get the exit code
exit_code = process.wait()

if exit_code == 0:
    print("Script executed successfully.")
else:
    print(f"Script execution failed with exit code: {exit_code}")

# Here comes the code to load the data
path = "data/data_analysis.pkl"

# Download the data
with open(path, 'rb') as file:
    data_analysis = pd.read_pickle(file)

# Global Administrative Areas
de = pygadm.get_items(admin="DEU", content_level=0)
path = "data/de.pkl"

with open(path, "wb") as file:
    pickle.dump(de, file)

with open(path, 'rb') as file:
    de = pd.read_pickle(file)

# Create grid of smoothed values 
knn_data = knn_smooth(df=data_analysis, country_df=de, variable="prec", 
           long="long", lat="lat", radius=1, grid_count=100)

sub_data = knn_data.dropna(subset=['prec'])

app_ui = ui.page_fluid(
        ui.h1("Current precipitation in Germany"),
        ui.markdown(
                """
                This html-application plots the most recent 10-min sum of precipitation in Germany using k-nearest 
                neighbour smoothing, the data stems from the [DWD (Deutscher Wetter Dienst)](https://opendata.dwd.de/climate_environment/CDC/).
                """
        ),
    ui.layout_sidebar(
        ui.panel_sidebar(
            ui.input_action_button("refreshButton", "Refresh Data"),
            ui.input_radio_buttons("type", "Plot type",
                choices = ["Points", "Interpolation"]
            ),
            ui.input_slider("radius", "Radius for the smoothing:", min=0.1, max=3, value=1, step=0.1)
        ),
        ui.panel_main(
            ui.output_plot("plot")
        )
    ),
    ui.output_text_verbatim("txt"),
    ui.output_text_verbatim("progress")
)

def server(input, output, session):
    data = reactive.Value(sub_data)
    tstamp = reactive.Value(data_analysis["date"][0])

    @reactive.Effect
    @reactive.event(input.refreshButton)
    def _():
        command = ['python', 'data_pipe.py']
        process = subprocess.Popen(command)
        process.wait()

        # Wait for the process to finish and get the exit code
        exit_code = process.wait()

        if exit_code == 0:
            print("Script executed successfully.")
        else:
            print(f"Script execution failed with exit code: {exit_code}")

        # Here comes the code to load the data
        path = "data/data_analysis.pkl"

        # Download the data
        with open(path, 'rb') as f:
            data_analysis = pd.read_pickle(f)

        # Create the grid of smoothed values
        knn_data = knn_smooth(df=data_analysis, country_df=de, variable="prec",
                              long="long", lat="lat", radius=input.radius(), grid_count=100)

        # Subsetting the data
        sub_data = knn_data.dropna(subset=['prec'])

        data.set(sub_data)
        tstamp.set(data_analysis["date"][0])

    @output
    @render.plot(alt = "A spatial plot.")
    def plot():
        if input.type() == "Interpolation":
            fig = plot_interpolation(data.get(), de, variable="prec", n=150)
        elif input.type() == "Points":
            fig = plot_points(data.get(), de, variable="prec", markersize=4)
        return fig

    @output
    @render.text
    @reactive.event(input.refreshButton, ignore_none=False)
    def txt():
        return f"Timestamp: {str(tstamp.get())}"

app = App(app_ui, server)