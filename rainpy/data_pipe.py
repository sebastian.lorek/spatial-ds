"""
Data pipe.
"""

# Dependencies
import requests
import os
from bs4 import BeautifulSoup
import zipfile
import pandas as pd
import pickle

# Create the directory name to save the files 
download_directory = "data/download"

# Create the "data" directory if it doesn't exist
if not os.path.exists(download_directory):
    os.makedirs(download_directory)

# Urls
url = "https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/10_minutes/precipitation/now/"
url_stations = "https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/10_minutes/precipitation/now/zehn_now_rr_Beschreibung_Stationen.txt"

# Send a GET request to the website 
response = requests.get(url)
response_stations = requests.get(url_stations)

 # Store all failed downloads in a list 
fail = []

# Obtain the station information 
name_stations = "zehn_now_rr_Beschreibung_Stationen.txt"
if response_stations.status_code == 200:
    file_path = os.path.join(download_directory, name_stations)
    with open(file_path, "wb") as file:
        file.write(response_stations.content)
    print(f"{name_stations} downloaded successfully.")
else:
    fail.append(name_stations)
    print(f"Failed to download {name_stations}.")

# Obtain the station level data
if response.status_code == 200:
    # Parse the HTML content
    soup = BeautifulSoup(response.content, "html.parser")

    # Find all <a> tags with filenames starting with "10minutenwerte_nieder_" and ending with ".zip"
    file_links = soup.find_all("a", href=lambda href: href and href.startswith("10minutenwerte_nieder_") and href.endswith(".zip"))

    # Download each file 
    for link in file_links:
        file_url = url + link["href"]
        file_name = link["href"]
        file_path = os.path.join(download_directory, file_name)

        print(f"Downloading {file_name}...")
        file_response = requests.get(file_url)
        if file_response.status_code == 200:
            with open(file_path, "wb") as file:
                file.write(file_response.content)
            print(f"{file_name} downloaded successfully.")
        else:
            fail.append(file_name)
            print(f"Failed to download {file_name}.")
else:
    print("Failed to retrive the website content.")

# Specify the directory to extract the contents of the ZIP file
extract_directory_metadata = "data/extract/metadata"
extract_directory = "data/extract"

# Create the extraction directory if it doesn't exist
if not os.path.exists(extract_directory):
    os.makedirs(extract_directory)
    
# Create the extraction directory for the metadata if it doesn't exist
if not os.path.exists(extract_directory_metadata):
        os.makedirs(extract_directory_metadata)

# Loop through all ZIPs and open
for link in file_links:
    file_name = link["href"]
    zip_file_path = os.path.join(download_directory, file_name)

    with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
        file_list = zip_ref.namelist()
        for file_name_list in file_list:
            if file_name_list.startswith("produkt_zehn_now_rr") and file_name_list.endswith(".txt"):
                zip_ref.extract(file_name_list, extract_directory)
            else:
                zip_ref.extract(file_name_list, extract_directory_metadata)
    print(f"{file_name} unzipped successfully.")

# Read in the station data file 
column_widths = [6, 9, 19, 8, 10, 8, 41, 50]
dtype_mapping = {7: str}
na_values = ['', ' ', 'NaN']
data_stations = pd.read_fwf(os.path.join(download_directory, name_stations), 
                 widths=column_widths, 
                 header=None, 
                 encoding="latin1", 
                 skiprows=2, 
                 dtype=dtype_mapping, 
                 na_values=na_values)

# Assign column names to the DataFrame
column_names = ["STATIONS_ID", "von_datum", "bis_datum", "Stationshoehe", "geoBreite", "geoLaenge", "Stationsname", "Bundesland"]
data_stations.columns = column_names

data_stations.reset_index(drop=True, inplace=True)

data_stations["von_datum"] = pd.to_datetime(data_stations["von_datum"], format="%Y%m%d")
data_stations["bis_datum"] = pd.to_datetime(data_stations["bis_datum"], format="%Y%m%d")

data_stations[["Stationshoehe", "geoBreite", "geoLaenge"]] = data_stations[["Stationshoehe", "geoBreite", "geoLaenge"]].astype(float)

# Save the data to use later 
save_directory = "data"
with open(os.path.join(save_directory, "data_stations.pkl"), "wb") as file:
    pickle.dump(data_stations, file)

dfs = []
# Loop through all files in extract 
for file_name in os.listdir(extract_directory):
    if file_name.startswith("produkt_zehn_now_rr_") and file_name.endswith(".txt"):
        file_path = os.path.join(extract_directory, file_name)
        df = pd.read_csv(file_path, sep=";", header=0)
        dfs.append(df)

# Merge all dfs 
merged_data = pd.concat(dfs)

# Safe the concatinated file 
with open(os.path.join(save_directory, "merged_data.pkl"), "wb") as file:
    pickle.dump(merged_data, file)

# Transform the column "MESS_DATUM" to a datetime format
merged_data.loc[:,"MESS_DATUM"] = pd.to_datetime(merged_data.loc[:,"MESS_DATUM"], format = "%Y%m%d%H%M")

# Obtain the most recent date
most_recent_date = merged_data.loc[:,"MESS_DATUM"].max()

# Filter the data to obtain the most recent date 
data = merged_data.loc[merged_data["MESS_DATUM"] == most_recent_date]

# Drop the eor column 
data = data.drop("eor", axis=1)

# Remove all errors (marked with -999 in the RWS_IND_10 column)
data = data.loc[data["RWS_10"] != -999]

# Left join to obtain the data for analysis 
data_analysis = pd.merge(data, data_stations, on=["STATIONS_ID"], how="left")

# Strip white space from the columns 
data_analysis.columns = data_analysis.columns.str.strip()

# Filter for relevant columns 
subset_columns = ["STATIONS_ID", "MESS_DATUM", "RWS_DAU_10", "RWS_10", "RWS_IND_10", "Stationshoehe", "geoLaenge", "geoBreite", "Stationsname", "Bundesland"]
data_analysis = data_analysis.loc[:,subset_columns]

# Rename the columns 
data_analysis.columns = ["id", "date", "prec_dur", "prec", "prec_ind", "alt", "long", "lat", "station_name", "state"]

# Safe the final file 
with open(os.path.join(save_directory, "data_analysis.pkl"), "wb") as file:
    pickle.dump(data_analysis, file)