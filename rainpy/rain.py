"""
Main script.
"""

import os
import numpy as np
import pandas as pd
import geopandas as gpd
from scipy.interpolate import griddata
from shapely.geometry import Point
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

# k-nearest-neighbour smoothing 
def knn_smooth(df: pd.DataFrame, country_df: gpd.GeoDataFrame, variable: str, long: str = "long", lat: str = "lat", radius: float = 10, grid_count: int = 100):

    min_longitude, min_latitude, max_longitude, max_latitude = country_df.total_bounds
    
    grid_long = np.linspace(min_longitude, max_longitude, grid_count)
    grid_lat = np.linspace(min_latitude, max_latitude, grid_count)
    grid1, grid2 = np.meshgrid(grid_long, grid_lat)
    data_grid = pd.DataFrame({long: grid1.flatten(), lat: grid2.flatten(), 
                              variable: np.empty(len(grid1.flatten())), "neighbours": np.empty(len(grid1.flatten()))})

    for i in range(data_grid.shape[0]):
        local_boolean = np.sqrt((df[long] - data_grid[long][i])**2 + (df[lat] - data_grid[lat][i])**2) < radius
        data_grid[variable][i] = np.mean(df[variable][local_boolean])
        data_grid["neighbours"][i] = np.sum(local_boolean)

    data_grid['geometry'] = data_grid.apply(lambda row: Point(row[long], row[lat]), axis=1)
    data_geo_return = gpd.GeoDataFrame(data_grid, geometry="geometry", crs="EPSG:4326")

    return data_geo_return

# plot smoothed points 
def plot_points(df: gpd.GeoDataFrame, country_df: gpd.GeoDataFrame, variable: str, **kwinputs):

    data_plot = gpd.sjoin(df, country_df, predicate="within")
    data_plot = data_plot[df.columns]
    
    plt.ioff()  # Turn off interactive mode to prevent plot display

    # Create the figure and axes
    fig, ax = plt.subplots(figsize=(8, 15))

    # Plot the borders
    country_df.plot(ax=ax, facecolor='white', edgecolor='black')

    # Plot the rainfall at the stations and get the legend object
    data_plot.plot(column=variable, ax=ax, legend=True, **kwinputs)
    ax.set_xlabel("long")
    ax.set_ylabel("lat")

    plt.close(fig)

    return fig 

def plot_interpolation(df: gpd.GeoDataFrame, country_df: gpd.GeoDataFrame, variable: str, n: int = 150, levels:int = 10, **kwinputs):

    # get the envelope of germany to make the plot there white 
    outside_country = gpd.GeoDataFrame(geometry=[country_df.unary_union.envelope])
    outside_country = outside_country.difference(country_df.unary_union)

    # Generate a grid of points
    x = df.geometry.x
    y = df.geometry.y
    z = df[variable]
    xi = np.linspace(x.min(), x.max(), n)
    yi = np.linspace(y.min(), y.max(), n)
    xi, yi = np.meshgrid(xi, yi)

    # Interpolate values on the grid
    zi = griddata((x, y), z, (xi, yi), method='cubic')

    plt.ioff() # Turn off interactive mode to prevent plot display

    # Create the figure and axes
    fig, ax = plt.subplots(figsize=(8, 15))

    # Plot the rainfall at the stations and get the legend object
    # Plot the interpolated surface as contour lines
    contourf = ax.contourf(xi, yi, zi, levels=levels)

    # Add colorbar
    plt.colorbar(contourf)

    # make everything outside of the border white 
    outside_country.plot(ax=ax, facecolor="white")

    # Plot the borders
    country_df.plot(ax=ax, facecolor='none', edgecolor='black')

    ax.set_ylabel("lat")
    ax.set_xlabel("long")

    plt.close(fig)

    return fig