# Rain prediction in Germany

## Project Idea

The idea of the project is to implement a spatial model that is able to create a smooth surface of rainfall through out Germany. In spatial data science it is common to have observations only at specific points in space also called pointed referenced data (i.e. rain at the weather stations in Germany). However we all know that rain is a continous phenomena through space and time which we have to account for, such that we are able to know the weather/rain prediction at our current GPS location (and ideally also consider the time aspect of weather data). So the goal is to explore statical methods and maybe later machine learning (probabilistic) models that are able to create smooth rain-surfaces over germany. The model output can be used for visualization and prediction. Data is readily available from the open data portal of the [DWD(Deutscher Wetter Dienst)](https://www.dwd.de/DE/leistungen/opendata/opendata.html) where we can download the data, administrative border data is also available by open data sources ([gadm](https://gadm.org/)). The first step is to download the data create some plotting functions and get to know the geo-data structures in python. After that start with a simple k-nearest neighbour smoother and expand from there step by step. Only the space domain with monthly data frequency will be considered. Such that the prediction functions outputs the predicted average monthly rainfall for a specific location and maybe some prediction interval. While the plotting function plots the interpolated rain surface for each month in the year for Germany. 

## Functionalities 

- Data pipe to obtain recent 10-min data from the DWD open data center :heavy_check_mark:
- Plotting function :heavy_check_mark:
- Interpolation function :heavy_check_mark:
- Create a shiny dashboard :heavy_check_mark:
- Full spatio-temporal model :x:

## Dev Notes:

### Execute the application

To run the shiny application you must have installed all required python packages from the requirements.txt file. You can set up the shiny dashboard by simply running `shiny run` while being the `rainpy` directory. After that you can access the dashboard by using the local link that is provided in the terminal.

### Packages 

List of helpful standard packages:
- `numpy`
- `pandas`
- `matplotlib`

Spatial related:
- [`Geopandas`](https://geopandas.org/en/stable/index.html)
- [`pygadm`](https://pygadm.readthedocs.io/en/stable/)
- [`shapely`](https://shapely.readthedocs.io/en/stable/manual.html)

What we could have used aswell:
- `Fiona`
- `PySAL`
- `osmnx`
- `plotly`
- `ipyleaflet`

### Data sources

- DWD data can be acessed [here](https://opendata.dwd.de/climate_environment/) and [here](https://dwd-geoportal.de/products/?query=%7B%22location%22%3Atrue,%22fulltext%22%3A%22Stationsdaten%22%7D)
- Administrative borders are available [here](https://gadm.org/)

### Some literature 

- [Quick introduction to Geopanas](https://geopandas.org/en/stable/getting_started/introduction.html)
- [Spatial Data Science with Apl. in R](https://r-spatial.org/book/)
- [python for Geospatial Data Analysis](https://github.com/gicait/python-for-geospatial-data-analysis)
- [youtube introduction](https://www.youtube.com/watch?v=KoLAlcBv290)

### Timeline 

![timeline](man/fig/timeline_python_ds.png)